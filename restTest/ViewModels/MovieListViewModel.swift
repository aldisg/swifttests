//
//  MovieListViewModel.swift
//  restTest
//
//  Created by Aldis Grauze on 16.05.21.
//

import Foundation
import CoreData

class MovieListViewModel {
    
    private let client = MoviesClient()
    
    func getAllMovies(completion: @escaping ([MovieViewModel]) -> Void) {
        
        let records: [MovieRecord] = MovieRecord.all()
        let movies = records.map(MovieViewModel.init)
        completion(movies)
    }
    
    func refreshMovies(completion: @escaping (AppError?)->Void) {
        
        fetchMovies(from: .topRated) {
            (results, error) in
            if let error = error {
                return completion(AppError.runtimeError(error.localizedDescription))
            }
            guard let results = results else {
                return completion(AppError.runtimeError("No new feeds"))
            }
            
            // parse JSON model to Repository model
            for result in results {
                
                // TODO check if such movie exists
                let movies: [MovieRecord] = MovieRecord.all(predicate: "%K == %@", args:["movie_id", "\(result.id)"])
                if let movie = movies.first {
                    // TODO do something with it - mark?
//                    movie.visited = true
                    print("movie exist")
                }
                else {
                    print("new movie")
                    let newMovie = self.createMovieFromJson(movie: result)
                    newMovie.save()
                }
            }
            completion(nil)
        }
        
    }
    
    // Web API ----------------------------
    
    private func fetchMovies(from movieFeedType: MoviesAPIEndpoint, completion: @escaping ([Movie]?, Error?)->Void ) {
        
        client.getFeed(from: movieFeedType) {
            result in
            
            switch(result){
            case .success(let fetchedMovies):
                completion(fetchedMovies?.results,nil)
            case .failure(let error):
                completion(nil,error)
            }
        }
    }
    
    private func createMovieFromJson(movie: Movie) -> MovieRecord{
        
        let movieRecord = MovieRecord(context: MovieRecord.viewContext)
        movieRecord.movie_id = Int32(Int64(movie.id))
        movieRecord.title = movie.title
        movieRecord.overview = movie.overview
        movieRecord.visited = false
        movieRecord.released = movie.release_date?.toDate(withFormat: "yyyy-MM-DD")
        movieRecord.poster = movie.poster_path
//        movieRecord.average = NSDecimalNumber(decimal: movie.average)
//        movieRecord.popularity = NSDecimalNumber(decimal: movie.popularity)
        movieRecord.topranking = 0
        return movieRecord
    }
}

// prepare movie for displaying and navigation
struct MovieViewModel {
    
    let movie: MovieRecord
    
    var id: NSManagedObjectID {
        return movie.objectID
    }
    
    var movieId: Int {
        return Int(movie.movie_id)
    }
    
    var title: String {
        return movie.title ?? ""
    }
    
    var overview: String {
        return movie.overview ?? ""
    }
    
    var released: String {
        return movie.released?.getFormattedDate(format: "MM.dd.yyyy") ?? "not available"
    }
    
    var visited: Bool {
        return movie.visited
    }
    
    var topranking: Int {
        return Int(movie.topranking)
    }
    
    var poster: String? {
        return movie.poster
    }
    
//    var popularity: Decimal {
//        return movie.popularity! as Decimal
//    }
//
//    var average: Decimal {
//        return movie.average! as Decimal
//    }
}
