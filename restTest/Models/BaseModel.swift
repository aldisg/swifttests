//
//  BaseModel.swift
//  restTest
//
//  Created by Aldis Grauze on 17.05.21.
//

import Foundation
import CoreData

protocol BaseModel: NSManagedObject {
    
    func save()
    func delete()
    static func byId<T: NSManagedObject>(id: NSManagedObjectID) ->T?
    static func all<T: NSManagedObject>(predicate: String?, args: [String]?) -> [T]
}

extension BaseModel{
    
    static var viewContext: NSManagedObjectContext {
        return CoreDataRepository.shared.viewContext
    }
    
    func save() {
        do {
            try Self.viewContext.save()
        }
        catch {
            print(error)
        }
    }
    
    func delete() {
        Self.viewContext.delete(self)
    }
    
    static func all<T>(predicate: String? = nil, args: [String]? = nil) -> [T] where T: NSManagedObject {
        
        let fetchRequest: NSFetchRequest<T> = NSFetchRequest(entityName: String(describing: T.self))
        if let predicate = predicate, let args = args {
            fetchRequest.predicate = NSPredicate(format: predicate, argumentArray: args)
        }
        
        do {
            return try viewContext.fetch(fetchRequest)
        }
        catch {
            return []
        }
    }
    
    static func byId<T>(id: NSManagedObjectID) -> T? where T: NSManagedObject {
        
        do {
            return try viewContext.existingObject(with: id) as? T
        }
        catch {
            print(error)
            return nil
        }
    }
    
}
