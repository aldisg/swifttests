//
//  Movie.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation

struct Movie: Decodable {
    
    let id: Int
    let title: String?
    let poster_path: String?
    let overview: String?
    let releaseDate: String?
    let backdrop_path: String?
    let release_date: String?
//    let average: Decimal
//    let popularity: Decimal
}

