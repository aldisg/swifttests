//
//  AppError.swift
//  restTest
//
//  Created by Aldis Grauze on 16.05.21.
//

import Foundation

enum AppError: Error {
    case runtimeError(String)
}

