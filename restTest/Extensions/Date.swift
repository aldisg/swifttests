//
//  Date.swift
//  restTest
//
//  Created by Aldis Grauze on 16.05.21.
//

import Foundation

extension Date {
    
   func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}
