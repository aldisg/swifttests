//
//  CoreDataManager.swift
//  restTest
//
//  Created by Aldis Grauze on 16.05.21.
//

import Foundation
import CoreData

class CoreDataRepository {
    
    let persistentContainer: NSPersistentContainer
    var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    static let shared = CoreDataRepository()
    
    private init() {
        
        persistentContainer  = NSPersistentContainer(name: "MovieDB")
        persistentContainer.loadPersistentStores { (description,error) in
            if let error = error {
                fatalError("Failed to initialize CoreData \(error.localizedDescription)!")
            }
        }
    }
    
    // CRUD
    func readAll() throws -> [MovieRecord]{
        
        let fetchRequest: NSFetchRequest<MovieRecord> = MovieRecord.fetchRequest()
        do {
            return try self.viewContext.fetch(fetchRequest)
        }
        catch {
            return []
        }
    }
    
    func save() {
        
        do {
            try self.viewContext.save()
        }
        catch {
            self.viewContext.rollback()
            print("Failed to save \(error.localizedDescription)")
        }
    }
    
}
