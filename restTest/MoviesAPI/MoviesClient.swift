//
//  MovieClient.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation
import UIKit

class MoviesClient: ClientAPI {

    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func getFeed(from movieFeedType: MoviesAPIEndpoint, completion: @escaping (Result<MoviesFeedResult?, ErrorAPI>) -> Void) {
        fetch(with: movieFeedType.request , decode: { json -> MoviesFeedResult? in
            guard let movieFeedResult = json as? MoviesFeedResult else { return  nil }
            return movieFeedResult
        }, completion: completion)
    }
    
    func getImage(from movieFeedType: MoviesAPIEndpoint, completion: @escaping (Result<UIImage?, ErrorAPI>) -> Void) {
        fetchImage(with: movieFeedType.request, completion: completion)
    }
}
