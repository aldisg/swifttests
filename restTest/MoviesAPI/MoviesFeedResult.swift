//
//  ResultMovieAPI.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation

struct MoviesFeedResult: Decodable {
    let results: [Movie]?
}
