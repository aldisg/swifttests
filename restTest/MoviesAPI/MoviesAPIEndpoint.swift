//
//  MovieFeed.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation

enum MoviesAPIEndpoint {
    case nowPlaying
    case topRated
    case image(resource: String?)
}
extension MoviesAPIEndpoint: EndpointAPI {
    
    var base: String {
        return "https://api.themoviedb.org"
    }
    
    var path: String {
        switch self {
            case .nowPlaying: return "/3/movie/now_playing"
            case .topRated: return "/3/movie/top_rated"
            case .image(let resource): return resource ?? ""
        }
    }
    
}
