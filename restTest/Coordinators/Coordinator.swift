//
//  Coordinator.swift
//  restTest
//
//  Created by Aldis Grauze on 11.05.21.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}
