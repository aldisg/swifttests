//
//  MainCoordinator.swift
//  restTest
//
//  Created by Aldis Grauze on 11.05.21.
//

import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = InfoListViewController.instantiate()
        navigationController.pushViewController(vc, animated: false)
    }
}
