import UIKit


let me = "acoms"
let repos = NetworkAPI.repos(username: me)
let firstRepo = repos.compactMap { $0.first }
let issues = firstRepo.flatMap { repo in
    NetworkAPI.issues(repo: repo.name, owner: me)
}

let token = issues.sink(receiveCompletion: { _ in print("completed")},
                       receiveValue: { print($0) })

