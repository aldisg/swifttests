//
//  NetworkAPI.swift
//  restTest
//
//  Created by Aldis Grauze on 09.05.21.
//

import Foundation
import Combine

enum NetworkAPI {
    static let agent = Agent()
    static let base = URL(string: "https://api.github.com")!
}

extension NetworkAPI {
    
    static func repos(username: String) -> AnyPublisher<[Repository], Error> {
        return run(URLRequest(url: base.appendingPathComponent("users/\(username)/repos")))
    }
    
    static func issues(repo: String, owner: String) -> AnyPublisher<[Issue], Error> {
        return run(URLRequest(url: base.appendingPathComponent("repos/\(owner)/\(repo)/issues")))
    }
    
    static func repos(org: String) -> AnyPublisher<[Repository], Error> {
        return run(URLRequest(url: base.appendingPathComponent("orgs/\(org)/repos")))
    }
    
    static func members(org: String) -> AnyPublisher<[User], Error> {
        return run(URLRequest(url: base.appendingPathComponent("orgs/\(org)/members")))
    }
    
    static func run<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, Error> {
        return agent.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }
}
