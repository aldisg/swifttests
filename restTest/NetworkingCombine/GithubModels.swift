//
//  Repository.swift
//  restTest
//
//  Created by Aldis Grauze on 09.05.21.
//

import Foundation

struct User: Codable {
    let id: Int
}

struct Repository: Codable {
    let id: Int
    let name: String
    let description: String?
}

struct Issue: Codable {
    let id: Int
}
