//
//  EndpointAPI.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation



enum Result<T, U> where U: Error  {
    case success(T)
    case failure(U)
}

protocol EndpointAPI {
    var base: String { get }
    var path: String { get }
}
extension EndpointAPI {
    var apiKey: String {
        return "api_key=1484c2b616b9af9478314bb70ee5bb6d"
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        components.query = apiKey
        return components
    }
    
    var request: URLRequest {
        let url = urlComponents.url!
        return URLRequest(url: url)
    }
}
