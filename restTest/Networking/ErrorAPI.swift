//
//  ErrorAPI.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation

enum ErrorAPI: Error {
    case requestFailed
    case jsonConversionFailure
    case dataConversionFailure
    case invalidData
    case responseUnsuccessful
    case jsonParsingFailure
    var localizedDescription: String {
        switch self {
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        case .dataConversionFailure: return "Data Conversion Failure"
        }
    }
}
