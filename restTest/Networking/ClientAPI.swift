//
//  ClientAPI.swift
//  restTest
//
//  Created by Aldis Grauze on 10.05.21.
//

import Foundation
import UIKit

protocol ClientAPI {
    var session: URLSession { get }
    func fetch<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, ErrorAPI>) -> Void)
    func fetchImage(with request: URLRequest, completion: @escaping (Result<UIImage?, ErrorAPI>) -> Void)
}

extension ClientAPI {
    
    typealias JSONTaskCompletionHandler = (Decodable?, ErrorAPI?) -> Void
    
    func fetchImage(with request: URLRequest, completion: @escaping (Result<UIImage?, ErrorAPI>) -> Void) {
        let task = session.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                DispatchQueue.main.async {completion(Result.failure(.requestFailed))}
                return
            }
            if httpResponse.statusCode == 200 {
                if let data = data {
                    let image = UIImage(data: data)
                    DispatchQueue.main.async {completion(Result.success(image))}
                } else {
                    DispatchQueue.main.async {completion(Result.failure(.invalidData))}
                }
            } else {
                DispatchQueue.main.async {completion(Result.failure(.responseUnsuccessful))}
            }
        }
        task.resume()
    }
    
    func fetch<T: Decodable>(with request: URLRequest, decode: @escaping (Decodable) -> T?, completion: @escaping (Result<T, ErrorAPI>) -> Void) {
            let task = decodingTask(with: request, decodingType: T.self) { (json , error) in
                
                //MARK: change to main queue
                DispatchQueue.main.async {
                    guard let json = json else {
                        if let error = error {
                            completion(Result.failure(error))
                        } else {
                            completion(Result.failure(.invalidData))
                        }
                        return
                    }
                    if let value = decode(json) {
                        completion(.success(value))
                    } else {
                        completion(.failure(.jsonParsingFailure))
                    }
                }
            }
            task.resume()
        }
    
    private func decodingTask<T: Decodable>(with request: URLRequest, decodingType: T.Type, completionHandler completion: @escaping JSONTaskCompletionHandler) -> URLSessionDataTask {
       
        let task = session.dataTask(with: request) { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse else {
                completion(nil, .requestFailed)
                return
            }
            if httpResponse.statusCode == 200 {
                if let data = data {
                    do {
                        let genericModel = try JSONDecoder().decode(decodingType, from: data)
                         completion(genericModel, nil)
                    } catch {
                        completion(nil, .jsonConversionFailure)
                    }
                } else {
                    completion(nil, .invalidData)
                }
            } else {
                completion(nil, .responseUnsuccessful)
            }
        }
        return task
    }
}
