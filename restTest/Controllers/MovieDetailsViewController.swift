//
//  MovieDetailsViewController.swift
//  restTest
//
//  Created by Aldis Grauze on 17.05.21.
//

import UIKit

class MovieDetailsViewController: UIViewController { 

    // temporary
    private let client = MoviesClient()
    
    var actualMovie: MovieViewModel?
    
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        client.getImage(from: .image(resource: actualMovie?.poster)) {
            result in
                print(result)
        }
        
        
        // Do any additional setup after loading the view.
        self.movieTitleLabel.text = actualMovie?.title
        self.movieDescriptionLabel.text = actualMovie?.overview
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
