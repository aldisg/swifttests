//
//  ViewController.swift
//  restTest
//
//  Created by Aldis Grauze on 05.05.21.
//

import UIKit

class InfoListViewController: UIViewController, Storyboarded {

    private var infoData:[MovieViewModel] = []
    
    private let listViewModel = MovieListViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reloadMovies), for: .valueChanged)

        self.tableView.refreshControl = refreshControl
        
        listViewModel.getAllMovies() {
            movies in
            self.infoData = movies
            self.tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let destination = segue.destination as? MovieDetailsViewController {
            let selectedRow = tableView.indexPathForSelectedRow!.row
            destination.actualMovie = self.infoData[selectedRow]
        }
    }
    
    @objc func reloadMovies(refreshControl: UIRefreshControl) {

        listViewModel.refreshMovies { [weak self] error in
            if let error = error {
                print(error.localizedDescription) 
            }
            else {
                self?.listViewModel.getAllMovies() {
                    movies in
                    self?.infoData = movies
                    self?.tableView.reloadData()
                    refreshControl.endRefreshing()
                }
            }
        }
    }
}

extension InfoListViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "WebInfoCell") as? WebInfoTableViewCell {
            let currentInfo = infoData[indexPath.row]
            cell.title.text = currentInfo.title
            cell.descriptionLabel.text = currentInfo.overview
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0;
    }
    
}

